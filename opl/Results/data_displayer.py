# -*- coding: utf-8 -*-
#!/usr/env/python3
import argparse
import csv

import matplotlib.pyplot as mpl


GRAPH_FILE = "graph.csv"

# DATA CSV INDEXES
DESIRED_EFFORT = 0
HAMMING_FROM_NODE = 1
HAMMING_TO_NODE = 2
EDIT_DIST_FROM_NODE = 3
EDIT_DIST_TO_NODE = 4


def load_graph():
    graph = {}

    with open(GRAPH_FILE, newline='') as csvfile:
        spamreader = csv.reader(csvfile, delimiter=',', quotechar='|')
        for row in spamreader:
            edge = list(map(int, row))
            graph[tuple(edge[:2])] = edge[2]

    return graph


def load_data(file):
    with open(file, newline='') as csvfile:
        spamreader = csv.reader(csvfile, delimiter=',', quotechar='|')
        return [tuple(map(int, row)) for row in spamreader]


def extract_info_from_data(graph, data):
    hamming_edges = []
    edit_distance_edges = []
    desired_effort = []

    for row in data:
        hamming_edges.append((row[HAMMING_FROM_NODE],
                              row[HAMMING_TO_NODE]))
        edit_distance_edges.append((row[EDIT_DIST_FROM_NODE],
                                    row[EDIT_DIST_TO_NODE]))
        desired_effort.append(row[DESIRED_EFFORT])

    hamming_efforts = [graph[edge] for edge in hamming_edges]
    edit_distance_efforts = [graph[edge] for edge in edit_distance_edges]

    return hamming_efforts, edit_distance_efforts, desired_effort


def display_data(graph, data):
    hamming, edit_dist, desired = extract_info_from_data(graph, data)
    length = len(hamming)

    x = [_ for _ in range(1, length + 1)]
    plot_1, = mpl.step(x, desired, "r-", linewidth=15, alpha=0.5)
    plot_2, = mpl.step(x, edit_dist, "b-", linewidth=10, alpha=1)
    plot_3, = mpl.step(x, hamming, "g-", linewidth=5, alpha=1)

    ax = mpl.subplot(111)
    box = ax.get_position()
    ax.set_position([box.x0, box.y0 + 0.1, box.width, box.height * 0.85])

    mpl.ylabel("Effort")
    mpl.xlabel("Temps (min)")
    mpl.figlegend((plot_1, plot_2, plot_3), ("Désiré", "Hamming", "Distance d'édition"),
                  loc="lower center", ncol=3, bbox_to_anchor=(0.5, 0.01))
    mpl.xlim(1, length)
    mpl.show()


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("data")
    args = parser.parse_args()

    graph = load_graph()
    data = load_data(args.data)

    display_data(graph, data)
