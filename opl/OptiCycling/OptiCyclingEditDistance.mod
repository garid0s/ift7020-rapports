/*********************************************
 * OPL 12.6.1.0 Model
 * Author: agariepy
 * Creation Date: 2015-04-15 at 5:12:33 PM
 *********************************************/
int NumberOfNodes = ...;
int MaximumTime = ...;
int StartAtNode = ...;
int StopAtNode = ...;

range TimeRange = 1..MaximumTime;

tuple arc {
	key int fromNode;
	key int toNode;
	int timeCost;
	int effort;
};
{arc} Arcs = ...;

dvar int+ L[a in Arcs][t in TimeRange] in 0..1;



int DesiredEffort[TimeRange] = ...;
int MaxEffort = 5;
dvar int RealEffort[TimeRange] in 0..MaxEffort;


//Variables for edit distance

dvar int Q[TimeRange][TimeRange] in 0..1;

tuple nodeEditDistance {
	key int i;
	key int j;
};

tuple arcEditDistance {
	key nodeEditDistance fromNode;
	key nodeEditDistance toNode;
	key int arcSetNumber;
};

{nodeEditDistance} d;
{arcEditDistance} H;
{arcEditDistance} D;
{arcEditDistance} V;
{arcEditDistance} EqPrime;

execute {
	for (var i=0; i<=MaximumTime; i++) {
		for (var j=0; j<=MaximumTime; j++) {
			d.add(i, j);
		}
	}
	for (var i=0; i<=MaximumTime; i++) {
		for (var j=1; j<=MaximumTime; j++) {
		    H.add(d.get(i, j-1), d.get(i, j), 0);
		}
	}
	for (var i=1; i<=MaximumTime; i++) {
		for (var j=0; j<=MaximumTime; j++) {
		    V.add(d.get(i-1, j), d.get(i, j), 1);
		}
	}
	for (var i=1; i<=MaximumTime; i++) {
		for (var j=1; j<=MaximumTime; j++) {
		    D.add(d.get(i-1, j-1), d.get(i, j), 2);
		    EqPrime.add(d.get(i-1, j-1), d.get(i, j), 3);
		}
	}
}

dvar float+ f[e in H union V union D union EqPrime] in 0..1;
dexpr float EditDistance = sum(s in H union V union D) f[s];
minimize EditDistance;

subject to {
	forall(k in TimeRange)
	oneArcPerTime:
	  sum(a in Arcs)L[a][k] == 1;
	 
	startNode:
	sum(a in Arcs : a.fromNode == StartAtNode)L[a][1] == 1;
	
	stopNode:
	sum(a in Arcs : a.toNode == StopAtNode)L[a][MaximumTime] == 1;
	
	forall(a in Arcs : a.fromNode == StopAtNode && a.toNode == StopAtNode, k in 1..(MaximumTime - 1))
	stayInStopState:
	L[a][k] <= L[a][k + 1];
	
	forall(j in 0..NumberOfNodes-1, k in 1..(MaximumTime - 1))
	flowConservation:
	sum(<i, j, c, e> in Arcs) L[<i, j, c, e>][k] == sum(<j, n, l, r> in Arcs) L[<j, n, l, r>][k + 1];
	
	forall(a in Arcs : a.fromNode != a.toNode, b in Arcs : a.fromNode == b.toNode, k in 1..(MaximumTime - 1))
	cantRepeatArc1:
	1 <= 2 - L[a][k] - L[b][k + 1];
	
	forall(a in Arcs : a.fromNode != a.toNode, b in Arcs : a.fromNode == b.toNode, k in 1..(MaximumTime - 2))
	cantRepeatArc2:
	1 <= 2 - L[a][k] - L[b][k + 2];

	forall(a in Arcs : a.fromNode != a.toNode, b in Arcs : a.fromNode == b.toNode, k in 1..(MaximumTime - 3))
	cantRepeatArc3:
	1 <= 2 - L[a][k] - L[b][k + 3];


	forall(i in TimeRange)
	findRealEffort:
	RealEffort[i] == sum(a in Arcs) L[a][i] * a.effort;

	//Edit distance constraints
	
	forall(i in TimeRange, j in TimeRange)
	equalityFlagInQ1:
	DesiredEffort[i] - RealEffort[j] <= MaxEffort * Q[i][j];
	
	forall(i in TimeRange, j in TimeRange)
	equalityFlagInQ2:
	DesiredEffort[i] - RealEffort[j] >= -1 * MaxEffort * Q[i][j];
	
	forall(<i, j> in d, e in EqPrime : e.toNode == <i, j>)
	avoidEqualityEdgeIfNotEqual:
	f[e] <= 1 - Q[i][j];
	
	forall(v in d diff {<0, 0>, <MaximumTime, MaximumTime>})
	editDistanceFlowConservation:
	sum(s in H union V union D union EqPrime : s.fromNode == v) f[s]
	  == sum(r in H union V union D union EqPrime : r.toNode == v) f[r];
	  
	flowOfSinkEditDistance:
	sum(s in H union V union D union EqPrime : s.toNode == <MaximumTime, MaximumTime>) 
		f[s] == 1;
};
