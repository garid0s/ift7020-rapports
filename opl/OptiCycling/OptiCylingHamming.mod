/*********************************************
 * OPL 12.6.1.0 Model
 * Author: agariepy
 * Creation Date: 2015-04-15 at 5:12:33 PM
 *********************************************/
int NumberOfNodes = ...;
int MaximumTime = ...;
int StartAtNode = ...;
int StopAtNode = ...;

range TimeRange = 1..MaximumTime;

tuple arc {
	key int fromNode;
	key int toNode;
	int timeCost;
	int effort;
};
{arc} Arcs = ...;

dvar int+ L[a in Arcs][t in TimeRange] in 0..1;

int DesiredEffort[TimeRange] = ...;
int MaxEffort = 5;
dvar int RealEffort[TimeRange] in 0..MaxEffort;

//Variables of Hamming distance with difference weighting

dvar int DifferenceOfEffort[TimeRange];
dvar int+ DifferenceOfEffortPlus[TimeRange];
dvar int+ DifferenceOfEffortMinus[TimeRange];

dexpr int SumOfDifferenceEffort = sum(i in TimeRange) (DifferenceOfEffortPlus[i] + DifferenceOfEffortMinus[i]);
minimize SumOfDifferenceEffort; 


subject to {
	forall(k in TimeRange)
	oneArcPerTime:
	  sum(a in Arcs)L[a][k] == 1;

	startNode:
	sum(a in Arcs : a.fromNode == StartAtNode)L[a][1] == 1;
	
	stopNode:
	sum(a in Arcs : a.toNode == StopAtNode)L[a][MaximumTime] == 1;
	
	forall(a in Arcs : a.fromNode == StopAtNode && a.toNode == StopAtNode, k in 1..(MaximumTime - 1))
	stayInStopState:
	L[a][k] <= L[a][k + 1];
	
	forall(j in 0..NumberOfNodes-1, k in 1..(MaximumTime - 1))
	flowConservation:
	sum(<i, j, c, e> in Arcs) L[<i, j, c, e>][k] == sum(<j, n, l, r> in Arcs) L[<j, n, l, r>][k + 1];
	
	forall(a in Arcs : a.fromNode != a.toNode, b in Arcs : a.fromNode == b.toNode, k in 1..(MaximumTime - 1))
	cantRepeatArc1:
	1 <= 2 - L[a][k] - L[b][k + 1];
	
	forall(a in Arcs : a.fromNode != a.toNode, b in Arcs : a.fromNode == b.toNode, k in 1..(MaximumTime - 2))
	cantRepeatArc2:
	1 <= 2 - L[a][k] - L[b][k + 2];

	forall(a in Arcs : a.fromNode != a.toNode, b in Arcs : a.fromNode == b.toNode, k in 1..(MaximumTime - 3))
	cantRepeatArc3:
	1 <= 2 - L[a][k] - L[b][k + 3];
	
	forall(i in TimeRange)
	findRealEffort:
	RealEffort[i] == sum(a in Arcs) L[a][i] * a.effort;
	
	//Weighted Hamming distance
	forall(i in TimeRange)
	calculateDifferenceOfEffort:
	DifferenceOfEffort[i] == RealEffort[i] - DesiredEffort[i];
	
	forall(i in TimeRange)
	handleNegativeDifference:
	DifferenceOfEffort[i] == DifferenceOfEffortPlus[i] - DifferenceOfEffortMinus[i];
};
