#How to compile

```bash
$ cd proposition
$ pdflatex proposition.tex
$ biber proposition.bcf
$ pdflatex proposition.tex
```
