%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%2345678901234567890123456789012345678901234567890123456789012345678901234567890
%        1         2         3         4         5         6         7         8

\documentclass[letterpaper, 11 pt, conference]{article}  
\usepackage[margin=2.5cm]{geometry}
                                                         
\newcommand{\HRule}{\rule{\linewidth}{0.5mm}}

\usepackage{graphicx}
\usepackage{float}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage[french]{babel}
\usepackage[backend=biber,style=ieee]{biblatex}
\bibliography{sources.bib}
\usepackage{tikz}
\usepackage{tikzscale}
\usepackage{caption}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{placeins}
\newcommand\numberthis{\addtocounter{equation}{1}\tag{\theequation}}
% \addto\captionsfrench{
%   \renewcommand{\labelitemi}{$\bullet$}
% }

\title{\LARGE \bf
    Circuit d'entraînement pour cycliste
}


\author{Alexandre Gariépy, Jean-Samuel Bédard et Mathieu Carpentier}% <-this % stops a space
\begin{document}

%TITLE PAGE
\begin{titlepage}
  \thispagestyle{empty}%
  \pagenumbering{roman}%
    \setcounter{page}{0}%                      % associe le numero "0" a la page titre
    \begin{center}
   \includegraphics[width=12em]{img/ulaval_logo} \\[2.5cm]
   \textsc{\Large Projet de recherche}\\[0.4cm]
   \textsc{\large IFT-7020}\\[0.4cm]
   \HRule \\[0.4cm]
   {\Huge \bfseries Circuit d'entraînement pour cycliste}
   \HRule \\ [2 cm]
   \textsc{pr\'{e}sent\'{e} \`{a}} \\[0.4cm]
   \begin{tabular}{l c l}
      {M. Claude-Guy Quimper} & - & Enseignant\\
   \end{tabular}
   \\ [1cm]
   \textsc{par} \\[0.5cm]
   Alexandre Gari\'{e}py\\
   Num\'{e}ro de dossier : 111 046 788\\ [0.25cm]
   Jean-Samuel Bédard\\
   Num\'{e}ro de dossier : 111 043 631\\[0.25cm]
   Mathieu Carpentier\\
   Num\'{e}ro de dossier : 111 044 647\\ 

      \vfill
      22 avril 2015 
       \vfill
       \large{D\'{e}partement d'informatique et de g\'{e}nie logiciel}\\
       \textsc{\large Facult\'{e} des sciences et de g\'{e}nie}\\
    \end{center}
  \clearpage%
  \let\maketitle\relax%
\end{titlepage}
\newpage


\maketitle
\thispagestyle{empty}
\pagestyle{empty}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{abstract}
    La qualité d'un parcours d'entraînement pour cycliste s'évalue par le fait qu'il répond aux besoins de l'athlète en terme de durée,
    de nombre de paliers d'efforts et de repos, etc. Dans cet article, nous nous intéressons à trouver un trajet d'entraînement
    optimal dans la carte d'un quartier donné qui respecte le mieux ce qui a été demandé par l'utilisateur. Pour ce faire, on
    modélise le problème dans un modèle de programmation linéaire et on compare les résultats de deux fonctions objectifs différentes.
\end{abstract}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{INTRODUCTION}
La recherche de chemin dans un graphe est un problème courant en informatique. Toutefois, le chemin recherché est pratiquement toujours le plus court. 
Quand ce n'est pas le cas, il s'agit souvent de trouver celui qui respecte une ou plusieurs contraintes.


Par exemple, dans \cite{Chen:2008:TTF:1373452.1373461}, on recherche le chemin le plus court qui satisfait une contrainte de délai, 
tandis que dans \cite{Oral:2012:MIP:2457525.2457706}, on cherche le meilleur trajet pour traverser rapidement et sécuritairement un terrain 
hostile en temps de guerre. Dans tous les cas, les chemins recherchés ne comportent pas de cycles et sont les plus courts possible.


Dans cet article, nous montrons un modèle linéaire permettant de trouver un parcours de cycliste dont la courbe d'effort 
correspond à celle qui était demandée. Dit autrement, nous trouvons un chemin de longueur variable comportant des cycles 
et dont l'effort réel pour effectuer ce parcours correspond le plus possible à ce qui était demandé au départ.


La section \ref{description} présente plus en détail les caractéristiques du problème à faire résoudre par notre modèle. 
Par la suite, la section \ref{approche} montre le modèle utilisé pour résoudre le problème et les différentes heuristiques 
utilisés pour évaluer le chemin. La section \ref{protocole} présente la façon dont nous avons testé celui-ci suivi de la 
section \ref{resultat} qui présente les résultats que nous avons obtenus. Finalement, la section \ref{discussion} fait part 
des conclusions auxquelles nous sommes arrivées suite aux expérimentations.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{DESCRIPTION DU PROBLÈME}
\label{description}

Le problème consiste à générer automatiquement un parcours d'entraînement pour un cycliste dans une zone prédéterminée et en fonction 
d'un graphique d'effort désiré dans le temps.

Plusieurs problèmes découlent de cette situation. Le premier est qu'on cherche un chemin de longueur approximative. 
En effet, celui-ci peut être légèrement plus long ou plus court que celui demandé. Le deuxième est que le parcours doit 
permettre les cycles. Cela est essentiel si on veut que le cycliste puisse revenir à son point de départ à la fin de l'entraînement. 
De plus, pour avoir un mouvement plus naturel, le chemin ne doit pas permettre de revenir sur ces pas. Cela oblige l'utilisation de cycles 
pour pouvoir visiter le même endroit plus d'une fois. Le modèle présenté dans la section \ref{approche} montre l'approche utilisée pour 
résoudre ces problèmes. 

Pour résoudre cette tâche, il faut aussi discrétiser la zone dans laquelle le cycliste s'entraînera. Cela est primordial pour pouvoir 
effectuer la recherche de chemin dans un graphe. Nous avons résolu ce problème en mettant un noeud sur chaque intersection et en reliant 
les noeuds selon les routes. De plus, nous avons supposé que chaque arête du graphe prend un temps équivalent à parcourir. Si la distance 
entre deux noeuds est trop longue, on ajoute des noeuds intermédiaires pour que le temps entre chaque noeud soit équivalent. Cela nous 
permet aussi de résoudre le problème des pentes qui sont plus longues à monter que descendre. En effet, il suffit d'ajouter des 
noeuds lors des montées.

Finalement, il reste le problème d'évaluer la pertinence du chemin trouvé. Pour ce faire, nous utilisons deux techniques: 
une distance de Hamming modifiée et une distance d'édition. Les détails sur la façon dont ces deux techniques ont été 
implémentées sont présentés dans la section \ref{approche}.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{APPROCHE PROPOSÉE}
\label{approche}
Le problème revient à une recherche de chemin dans un graphe orienté. Le chemin commence à un noeud choisi $\alpha$
et se termine à un noeud choisi $\beta$. $\alpha$ et $\beta$ peuvent être égaux. Au noeud $\beta$, on ajoute une boucle
de coût nul permettant de considérer des chemins de plusieurs longueurs différentes.


Chaque arête est étiquetée de deux valeurs. La première est
le coût en temps pour parcourir l'arête. Pour simplifier le problème, chaque arête du graphe 
a un coût égal. Ainsi, on peut omettre ces valeurs dans la représentation du graphe. Aussi,
cela permet d'avoir une correspondance directe entre le nombre de noeuds parcourus et le temps écoulé.
La deuxième étiquette est l'effort nécessaire pour parcourir l'arête. Cet effort est un
nombre entier fixé qualitativement:

\begin{itemize}
    \item 1 représente un effort léger, voire nul, lorsque le cycliste descend une pente.
    \item 2 représente un effort normal, c'est-à-dire l'effort nécessaire pour parcourir un terrain plat.
    \item 3 représente un effort légèrement plus élevé que la normale, qui peut être causé par exemple par un faux plat.
    \item 4 représente un effort élevé, nécessaire par exemple pour monter une pente peu abrupte.
    \item 5 représente un effort très élevé, nécessaire par exemple pour monter une pente abrupte.
    \item 0 signifie que l'entraînement est terminé. Cette étiquette n'est possible que sur la boucle autour du noeud $\beta$.
\end{itemize}

Soit $a$ une chaîne de nombres entiers représentant l'effort désiré par unité de temps et $b$ une autre
chaîne d'entiers représentant l'effort réel du chemin trouvé dans le graphe, le but
est de minimiser la distance entre $a$ et $b$. Ces deux chaînes sont de longueur $T$, soit le nombre
d'unités de temps maximal de l'entraînement. On remarque que l'entraînement peut être plus court que $T$,
auquel cas la fin de la chaîne sera constituée de 0.
Deux mesures de distance sont utilisées. 


La première mesure est la distance de Hamming à laquelle
on ajoute une pondération. Plus spécifiquement, $\sum\limits_{i=1}^L |a_i - b_i|$. On remarque
que l'on compare les éléments de même position des deux chaînes. On compte le nombre d'éléments différents.
Si les éléments sont différents, à la place d'ajouter 1 à la distance comme pour la distance de Hamming standard,
on ajoute la différence en valeur absolue entre $a_i$ et $b_i$.

La deuxième mesure est la distance d'édition. La distance d'édition compte le nombre d'insertion, de suppression et de 
remplacements pour passer d'une chaîne $a$ à une chaîne $b$. L'approche usuelle pour calculer la distance d'édition
est un algorithme de programmation dynamique. Soit $d_{i,j}$ la distance d'édition entre la sous-chaîne formée
des $i$ premiers caractères de $a$ et des $j$ premiers caractères de $b$. On a relation de récurrence:

$
d_{i,j} = 
\begin{cases} 
	i &\mbox{si } j = 0 \\ 
	j & \mbox{si } i = 0\\
    min(d_{i-1,j}, d_{i,j-1}, d_{i-1,j-1}+) & \mbox{sinon }

\end{cases} 
$

Comme le montre \cite{TMoisan:MIP:EditDistance}, il est possible de modéliser la distance d'édition dans un programme linéaire.


Ainsi, deux approches différentes sont proposées pour comparer un trajet réel avec un trajet désiré. Par contre, dans
les deux cas, les contraintes modélisant le mouvement du cycliste dans le graphe sont les mêmes; seuls le calcul de distance
et la fonction objectif changent. Donc, dans un premier temps, voici les contraintes communes aux deux modèles:\\
\begin{itemize}
    \item Soit $Arcs$, l'ensemble de tous les arcs du graphe. Les éléments de cet ensemble contiennent 4 valeurs: 
        le numéro du noeud de départ $dep$, le numéro du noeud de destination $dest$, le coût en temps de l'arc $cout$ 
        et le coût en effort de l'arc $effort$.\\
    \item Soit $T$, la longueur maximale d'un chemin.\\
    \item Soit $L$ , une matrice de booléens de dimension $|Arcs| \times T$ dont $L[i][j]$ indique si oui ou non l'arc $i$ est parcouru au temps $j$\\
\end{itemize}
On a les contraintes:
\begin{align*}
    \sum_{arc\,\in Arcs} L_{arc, k} = 1\ &\;\;\forall 1 \leq k \leq T \numberthis \label{unParTemps}\\
    \sum_{arc\,\in Arcs | arc.dep = \alpha} L_{arc, 1} = 1 \numberthis \label{departAAlpha}&\\
    \sum_{arc\,\in Arcs | arc.dest = \beta} L_{arc, 1} = 1 \numberthis \label{finABeta}&\\
    L_{arc, k} \leq  L_{arc, k+1} & \;\;\forall arc\,\in Arcs \,|\, arc.dep = arc.dest = \beta \numberthis \label{resteEtatTermine}\\
                                  & \;\;\forall 1 \leq k \leq T - 1\\
    \sum_{x\,\in Arcs} L_{x, k} = \sum_{\substack{y\,\in Arcs \\ | x.dest = y.dep}} L_{y, k+1} 
                                & \;\;\forall 1 \leq k \leq T - 1 \numberthis \label{flotVelo}\\
    1 \geq 2 - L_{x, k} - L_{y, k+1} & \;\;\forall x, y\,\in Arcs \,|\, x.dep \neq x.dest \wedge y.dest = x.dep \numberthis \label{pasDans1Temps}\\
                                     & \;\;\forall 1 \leq k \leq T - 1\\
    1 \geq 2 - L_{x, k} - L_{y, k+2} & \;\;\forall x, y\,\in Arcs \,|\, x.dep \neq x.dest \wedge y.dest = x.dep \numberthis \label{pasDans2Temps}\\
                                     & \;\;\forall 1 \leq k \leq T - 2\\
    1 \geq 2 - L_{x, k} - L_{y, k+3} & \;\;\forall x, y\,\in Arcs \,|\, x.dep \neq x.dest \wedge y.dest = x.dep \numberthis \label{pasDans3Temps}\\
                                     & \;\;\forall 1 \leq k \leq T - 3\\
    b_i = \sum_{arc\,\in Arcs} L_{arc, i} \times arc.effort & \;\;\forall 1 \leq k \leq T \numberthis \label{trouveEfforts}\\
\end{align*}

\eqref{unParTemps} assure que le cycliste n'est qu'à un seul endroit par unité de temps. \eqref{departAAlpha} et \eqref{finABeta} assurent que le trajet
commence bien au noeud $\alpha$ et se termine bien au noeud $\beta$, respectivement. \eqref{resteEtatTermine} assure qu'une fois qu'on entre dans
la boucle autour de $\beta$, on y reste jusqu'au temps $T$. Autrement dit, une fois l'entraînement terminé, le cycliste ne repart pas.
La contrainte \eqref{flotVelo} assure après avoir parcouru un arc, le prochain arc à parcourir part bel et bien du noeud d'arrivée de l'arc précédent.
Les contraintes \eqref{pasDans1Temps}, \eqref{pasDans2Temps} et \eqref{pasDans3Temps} assurent qu'on ne revienne pas au même noeud
avant trois unités de temps. En pratique, cela s'avère suffisant pour éviter que le cycliste ne cesse de faire demi-tour. Une exception
est ajoutée à ces trois contraintes pour la boucle autour de $\beta$. Finalement, \eqref{trouveEfforts} met les efforts réels dans le vecteur $b$.

\subsection{Distance de Hamming}
Regardons comment on trouve le trajet optimal en utilisant la distance de Hamming pondérée. On utilise les variables:
\begin{itemize}
    \item Soit $D$ un vecteur de longueur $T$ contenant la différence entre $a_i$ et $b_i$.\\
    \item Soit $D^+$ et $D^-$ des vecteurs positifs de taille $T$ utilisés pour représenter les valeurs absolues.\\
\end{itemize}
En utilisant la distance de Hamming, on souhaite minimiser les distances entre les efforts désirés et les efforts réels.
\begin{displaymath}
    \min \sum_{k=1}^T D_k^+ + D_k^-
\end{displaymath}
Pour ce faire, on ajoute les contraintes suivantes au modèle:\\
\begin{align*}
    D_k = b_k - a_k &\;\;\forall 1 \leq k \leq T\\
    D_k = D_k^+ - D_k^- &\;\;\forall 1 \leq k \leq T
\end{align*}

\subsection{Distance d'édition}
Si on veut utiliser la distance d'édition, la fonction objectif change. Comme proposé dans \cite{TMoisan:MIP:EditDistance},
on construit un graphe $g$ de $(T+1) \times (T+1)$ noeuds, notés $d_{i,j}$ avec des indices $i$ et $j$ allant de 0 à $T$.
On organise les noeuds en une grille carrée. 
Pour chaque paire de noeuds adjacents horizontalement dans la grille, on ajoute ajoute un arc de coût 1
allant de $d_{i,j-1}$ à $d_{i,j}$ dans un ensemble $H$. 
Pour chaque paire de noeuds adjacents verticalement dans la grille, on ajoute ajoute un arc de coût 1
allant de $d_{i-1,j}$ à $d_{i,j}$ dans un ensemble $V$.
Pour chaque paire de noeuds $d_{i-1,j-1}, d_{i,j}$, on ajoute ajoute un arc de coût 1
allant de $d_{i-1,j-1}$ à $d_{i,j}$ dans un ensemble $D$ et un autre arc de coût 0 allant lui aussi de $d_{i-1,j-1}$ à $d_{i,j}$ dans un autre
ensemble $E_q^\prime$.


La distance d'édition entre les chaînes $a$ et $b$ correspond à la longueur au chemin le plus court entre $d_{0,0}$ et $d_{T,T}$ 
dans le graphe $g$ contenant les arêtes des ensembles $H$, $V$,
$D$ et $E_q^\prime$. Par contre, on ne peut emprunter un arc (de coût nul) de l'ensemble $E_q^\prime$ entre $d_{i-1,j-1}$ et $d_{i,j}$
uniquement que si $a_i = b_j$.

Le modèle linéaire en conforme à celui proposé par \cite{TMoisan:MIP:EditDistance}:
\begin{itemize}
    \item Soit $Q$ une matrice de 0 et de 1 de taille $T$ par $T$ dont $Q_{i,j} = 1$ si et seulement si $a_i \neq b_j$.
    \item Soit $f(x)$ une fonction qui retourne 1 si et seulement si l'arête $x$ du graphe $g$ est sur le chemin le plus court ente $d_{0,0}$ et $d_{T,T}$.
    \item $\gamma = 5$ le plus grand nombre qu'on retrouve dans les chaînes $a$ et $b$.
\end{itemize}

La fonction objectif est:
\begin{displaymath}
    \min \sum_{x \in H \cup V \cup D} f(x)
\end{displaymath}
Sujet à, pour tout $1 \leq i \leq T$ et $1 \leq j \leq T$:
\begin{align*}
    &a_i - b_j \leq \gamma Q_{i,j} \\
    &a_i - b_j \geq -\gamma Q_{i,j} \\
    &f(x) \leq 1 - Q_{i,j} \; \forall x \in E_q^\prime \:tel\:que\: x.dest = d_{i,j} \\
\end{align*}
et sujet à, pour tout noeud $n$ sauf $d_{0,0}$ et $d_{T,T}$:
\begin{displaymath}
    \sum\limits_{\substack{w \in H \cup V \cup D \cup E_q^\prime \\ | w.dep = n}} f(w)
    = \sum\limits_{\substack{z \in H \cup V \cup D \cup E_q^\prime \\ | z.dest = n}} f(z)
\end{displaymath}
et sujet à
\begin{displaymath}
    \sum\limits_{\substack{z \in H \cup V \cup D \cup E_q^\prime \\ | z.dest = d_{T,T}}} f(z)
        = 1
\end{displaymath}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{PROTOCOLE D'EXPÉRIMENTATION}
\label{protocole}

Pour tester nos différents modèles, à l'aide de données géographiques provenant de Google Map, nous avons discrétisé une carte
dans un graphe orienté en ne considérant que les principales voies cyclables. Nous avons pris la peine de prendre un quartier qui 
comporte beaucoup de pentes dans les différentes routes pour simuler une zone d'entraînement réaliste.

\begin{figure}[ht]
    \centering
    \includegraphics[width=0.5\textwidth]{img/full_graph.png}
    \caption{Quartier résidentiel}
    \label{full_graph}
\end{figure}

Pour chaque route sur la carte, nous retrouvons au minimum 2 arêtes en sens opposé pour représenter les deux sens 
possibles de la route. Sur les différents arcs du graphe de la figure \ref{full_graph}, les flèches rouges représentent
une ascension et les vertes une descente; les arêtes en bleu représentent un chemin plat. De plus, chaque arc
représente en moyenne un temps de déplacement de 1 minute. Nous retrouvons donc un graphe à 48 noeuds
et 88 arêtes.

Comme instance de test, nous avons déterminé une courbe représentant l'effort désiré à chaque unité de temps de 1 minute
pour un trajet de 40 minutes. Le problème revient à déterminer un cycle dans le graphe fourni en tentant de s'approcher 
le plus possible de l'effort désiré à chaque unité de temps. Les modèles présentés dans la section \ref{approche} ont été
implémentés dans le solveur linéaire \textit{CPLEX}.


%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{RÉSULTATS}
La figure \ref{graphEffort} présente les résultats obtenus avec les deux différentes fonctions objectif présentées
dans la section \ref{approche}. Pour la distance de Hamming, la valeur optimale de la fonction objectif est 18. Cette
solution optimale a été trouvée en 5.88 secondes sur un processeur Intel i7-3610QM.

Pour la distance d'édition, la valeur optimale de la fonction objectif est 13. Cette solution a été trouvée
en 12 minutes et 19.85 secondes sur le même processeur Intel i7-3610QM.

\label{resultat}
\begin{figure}[ht]
    \centering
    \includegraphics[width=0.8\textwidth]{img/plot_comparaison.png}
    \caption{Effort voulu et efforts obtenus en fonction du temps}
    \label{graphEffort}
\end{figure}

La figure \ref{cartesTrajets} montre visuellement les trajets obtenus. Puisqu'un trajet peut passer plusieurs
fois par le même arc, trois couleurs sont utilisées pour tracer les trajets pour qu'on
puisse bien distinguer les étapes du trajet. Le trajet commence en jaune, puis continue en orange, et se termine en vert.

\begin{figure}[ht]
    \centering
    \includegraphics[width=0.45\textwidth]{img/graph_hamming.png}
    \includegraphics[width=0.45\textwidth]{img/graph_edit_distance.png}
    \caption{Trajets obtenus, à gauche en utilisant la distance de Hamming, à droite en utilisant la distance d'édition}
    \label{cartesTrajets}
\end{figure}


\FloatBarrier
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{DISCUSSION}
\label{discussion}

En observant la figure \ref{graphEffort}, on peut comparer les différences entre la distance de Hamming et la distance d'édition.
Cela est essentiel, car on ne peut pas comparer directement les valeurs objectives retournées par les deux méthodes. En effet, la distance
de Hamming est pondérée. Cela signifie qu'un effort demandé de 3 comparé à un effort réel de 1 compte pour 2. En utilisant la distance
d'édition, une addition, suppression, etc. compte toujours uniquement pour un.

Notons que, dans les deux cas, le graphe d'effort obtenu suit relativement bien la courbe demandée. Toutefois, les deux méthodes ont
aussi leurs inconvénients.
Dans le cas de Hamming, la courbe d'effort reste collée sur ce qui était demandé ce qui est excellent. 
De plus, cette méthode est beaucoup plus rapide pour retourner une solution que la distance d'édition.
Par contre, certains paliers d'efforts sont complètement ignorés. Par exemple, entre les minutes 30 et 33, un effort de 3 était demandé et le chemin trouvé
n'en a pas tenu compte.

Dans le cas de la distance d'édition, la courbe d'effort ne suit pas exactement ce qui était demandé. Par contre, elle suit 
beaucoup plus << l'esprit >> de ce que l'utilisateur souhaite faire. Par exemple, entre les minutes 17 et 27, il fallait faire un effort léger,
suivi d'une descente avant de refaire un effort léger. La distance d'édition a trouvé un chemin qui demandait de faire un effort léger,
suivi d'un plat avant de refaire un effort léger. De plus, cette méthode permet d'avoir une courbe d'effort décalée par rapport
à ce qui était demandé. Par exemple, aux minutes 22 à 26, le même palier que celui demandé est présent, mais décalé d'une minute. Ceci est un avantage
par rapport à la méthode de Hamming qui n'aurait pas pu le faire.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{CONCLUSION}
Dans cet article, nous avons présenté un modèle permettant de trouver un parcours
d'entraînement dans un quartier donné. Celui-ci permet notamment de trouver un chemin de 
longueur indéterminé et comportant des cycles qui correspond le mieux à ce qui était
demandé au départ en terme d'effort. De plus, pour évaluer la pertinence du chemin trouvé, 
nous avons utilisé deux techniques différentes, soit une distance de Hamming modifiée et une distance d'édition.

Dans le futur, il serait intéressant de trouver le parcours d'entraînement
qui fournit l'effort le plus semblable à ce qui était demandé et qui, en même temps,
maximise la couverture du quartier. C'est-à-dire que le cycliste visite le plus de 
noeuds différents possibles afin de diversifier le parcours.

De plus, dans le cas des athlètes de haut niveau qui sont capables de parcourir de 
très grandes distances en peu de temps, il faut modifier la technique afin que le 
solveur puisse trouver une solution dans un temps raisonnable. On pourrait s'appuyer sur
le travail de \cite{Mekni:2010:HPP:1808143.1808182} pour résoudre ce problème. L'idée 
consisterait à trouver le chemin dans une zone très vaste, mais dont la majorité des
routes ne sont pas marquées pour, par la suite, faire fonctionner le solveur sur les zones plus 
petites qui ont été traversées.

\nocite{*}
\printbibliography




\end{document}
